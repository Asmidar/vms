﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VMStest.Migrations
{
    public partial class EditorMsg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "msg1",
                table: "board",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "msg2",
                table: "board",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "msg3",
                table: "board",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "msg4",
                table: "board",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "msg5",
                table: "board",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "msg1",
                table: "board");

            migrationBuilder.DropColumn(
                name: "msg2",
                table: "board");

            migrationBuilder.DropColumn(
                name: "msg3",
                table: "board");

            migrationBuilder.DropColumn(
                name: "msg4",
                table: "board");

            migrationBuilder.DropColumn(
                name: "msg5",
                table: "board");
        }
    }
}
