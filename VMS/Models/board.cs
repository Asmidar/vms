﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VMStest.Models
{
    //VMS Board Model
    public class board
    {
        public int id { get; set; }
        public int zone { get; set; }
        public string tag { get; set; }
        public string location { get; set; }

        //Store URL of board current image for display
        public string image { get; set; }


        //Store URL of editor message 1-5 for looping 
        public string msg1 { get; set; }
        public string msg2 { get; set; }
        public string msg3 { get; set; }
        public string msg4 { get; set; }
        public string msg5 { get; set; }

    }
}
