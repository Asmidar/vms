﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace VMStest.Models
{
    public class myDbContext : DbContext
    {
        public myDbContext(DbContextOptions<myDbContext> options)
            : base(options)
        {
        }

        //The database context class, myDbContext, specifies which entities are included in the data model
        public DbSet<VMStest.Models.board> board { get; set; }
        public DbSet<VMStest.Models.history> history { get; set; }
    }
}


