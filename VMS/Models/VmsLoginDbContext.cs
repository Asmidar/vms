﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
//using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace VMStest.Models
{

    public class VmsLoginDbContext : DbContext
    {
        public VmsLoginDbContext(DbContextOptions<VmsLoginDbContext> options) : base(options) { }
        public DbSet<user> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
    }
}
