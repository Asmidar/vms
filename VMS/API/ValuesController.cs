﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIlibrary;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VMSEditor.API
{
    [Route("api/upload")]
    public class ValuesController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        /// <summary>
        /// 
        /// </summary>
        public ValuesController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public class PostStringModel
        {
            public string Value { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        [AllowAnonymous]
        [HttpPost("")]
        public IActionResult Upload()
        {
            var imageUtils = UploadUtils.Instance(_hostingEnvironment);

            var files = HttpContext.Request.Form.Files;
            if (files.Count == 0)
                throw new Exception("No file is selected");

            var file = files[0];

            var relativePath = imageUtils.Save(file.ToString());

            return Ok(new
            {
                Success = true,
                relativePath
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [AllowAnonymous]
        [HttpPost("base64")]
        public IActionResult UploadWithBase64([FromBody] PostStringModel model)
        {
            var imageUtils = UploadUtils.Instance(_hostingEnvironment);

            var relativePath = imageUtils.Save(model.Value);

            return Ok(new
            {
                relativePath
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [AllowAnonymous]
        [HttpPost("delete")]
        public IActionResult Delete()
        {
            return Ok(new
            {
                Success = true
            });
        }
    }
}
