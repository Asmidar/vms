﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
//Added
using Microsoft.EntityFrameworkCore;
using VMStest.Models;

namespace VMStest.Pages.VMS
{
    public class EditorModel : PageModel
    {
        private readonly VMStest.Models.myDbContext _context;

        public EditorModel(VMStest.Models.myDbContext context)
        {
            _context = context;
        }

        //Show message upon saving and existing editor
        [TempData]
        public string Message { get; set; }


        [BindProperty]
        public board Info { get; set; }
        //[BindProperty]
        //public history dt { get; set; }





        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Info = await _context.board.FirstOrDefaultAsync(m => m.id == id);

            if (Info == null)
            {
                return NotFound();
            }
            return Page();

        }

        public async Task<IActionResult> OnPostAsync()
        {
            Info.image = "http://localhost:54324/uploads/message1.jpg";
           

            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Info).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InfoExists(Info.id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            ////Current Date and Time
            //dt.nowdatetime = DateTime.Now;

            //_context.history.Add(dt);
            //await _context.SaveChangesAsync();

            Message = "Successfully Edited. Please check history.";


            return RedirectToPage("/Zones/Table1");
        }

        private bool InfoExists(int id)
        {
            return _context.board.Any(e => e.id == id);
        }
    }
}