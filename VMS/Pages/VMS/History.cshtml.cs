﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
//Added
using Microsoft.EntityFrameworkCore;
using VMStest.Models;

namespace VMStest.Pages.VMS
{
    public class HistoryModel : PageModel
    {
        private readonly VMStest.Models.myDbContext _context;
        public HistoryModel(VMStest.Models.myDbContext context)
        {
            _context = context;
        }

        public IList<history> Info { get; set; }

        public async Task OnGetAsync()
        {
            Info = await _context.history.ToListAsync();
        }
    }
}