﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
//Added
using Microsoft.EntityFrameworkCore;
using VMStest.Models;

namespace VMStest.Pages
{
    public class Table1Model : PageModel
    {
        private readonly VMStest.Models.myDbContext _context;
        public Table1Model(VMStest.Models.myDbContext context)
        {
            _context = context;
        }

        [TempData]
        public string Message { get; set; }
        public bool ShowMessage => !string.IsNullOrEmpty(Message);

        public IList<board> vms { get; set; }

        //HttpGET - retrieve data from database
        public async Task OnGetAsync()
        {
            vms = await _context.board.ToListAsync();
        }
    }
}