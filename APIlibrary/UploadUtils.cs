﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APIlibrary
{
    public class UploadUtils
    {
        private static readonly string[] Extensions = { ".jpeg", ".jpg", ".png", "mp4", "rar", "zip", "txt", "xlsx", "xlsm", "xlsb", "xltx", "xltm", "xls", "xlt", "xls", "xml", "pdf", "xlam", "csv" };
        private static readonly string[] ExtensionsImage = { ".jpeg", ".jpg", ".png" };
        private const long FileSize = 20000000;
        private const string UploadFolder = "uploads";
        private const string DefaultExtension = "png";

        private readonly IHostingEnvironment _hostingEnvironment;
        private static UploadUtils _itself;
        private static readonly object _lock = new object();

        private UploadUtils(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public static UploadUtils Instance(IHostingEnvironment hostingEnvironment)
        {
            if (_itself == null)
            {
                lock (_lock)
                {
                    if (_itself == null)
                    {
                        _itself = new UploadUtils(hostingEnvironment);
                    }
                }
            }

            return _itself;
        }

        internal object Save(IFormFile file)
        {
            throw new NotImplementedException();
        }

        public string Save(string base64)
        {
            try
            {
                lock (_lock)
                {
                    var fileName = "";
                    if (base64[0] == '1')
                    {
                        //fileName = $"message1.{DefaultExtension}";
                        //base64 = base64.Remove(0, 1);

                        base64 = base64.Remove(0, 1);
                        var newfilename = base64.Split('-')[0];
                        fileName = $"{newfilename}_message1.{DefaultExtension}";
                        base64 = base64.Split('-')[1];
                    }
                    else if (base64[0] == '2')
                    {
                        base64 = base64.Remove(0, 1);
                        var newfilename = base64.Split('-')[0];
                        fileName = $"{newfilename}_message2.{DefaultExtension}";
                        base64 = base64.Split('-')[1];
                    }
                    else if (base64[0] == '3')
                    {
                        base64 = base64.Remove(0, 1);
                        var newfilename = base64.Split('-')[0];
                        fileName = $"{newfilename}_message3.{DefaultExtension}";
                        base64 = base64.Split('-')[1];
                    }
                    else if (base64[0] == '4')
                    {
                        base64 = base64.Remove(0, 1);
                        var newfilename = base64.Split('-')[0];
                        fileName = $"{newfilename}_message4.{DefaultExtension}";
                        base64 = base64.Split('-')[1];
                    }
                    else if (base64[0] == '5')
                    {
                        base64 = base64.Remove(0, 1);
                        var newfilename = base64.Split('-')[0];
                        fileName = $"{newfilename}_message5.{DefaultExtension}";
                        base64 = base64.Split('-')[1];
                    }
                    else
                    {
                        fileName = "noname";
                    }


                    var uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, UploadFolder);

                    if (!Directory.Exists(uploadFolder)) Directory.CreateDirectory(uploadFolder);

                    var fileAbsolutePath = Path.Combine(uploadFolder, fileName);

                    var imageBytes = Convert.FromBase64String(base64);
                    File.WriteAllBytes(fileAbsolutePath, imageBytes);

                    var relativePath = $"{UploadFolder}/{fileName}";

                    return relativePath;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }
    }
}
